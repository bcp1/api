import pytest
import cv2

from src.main import app

@pytest.fixture
def image():

    with open("/usr/src/api/tests/image/lena.png", 'rb') as f:
        image = f.read()

    return image

@pytest.fixture(autouse=True)
def db_SetUp():
    app.config['BASE'].metadata.drop_all()
    app.config['BASE'].metadata.create_all()

@pytest.fixture(autouse=True)
def db_TearDown():
    yield
    app.config['BASE'].metadata.drop_all()