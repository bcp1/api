import base64
from io import BytesIO
import time

import pytest
import cv2

from src.common.classes import Card
from src.common.config import session
from src.services.cards import save_cards

def test_save_image(image):

    name = 'lena.png'

    dst_base64 = base64.b64encode(image)

    save_cards([{'name': name, 'image': dst_base64}])

    assert 1 == 1
