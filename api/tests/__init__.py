import pathlib
import sys
import os

sys.path.append(str(pathlib.Path(__file__).parents[0]))
os.environ['APP_ENV'] = 'test'