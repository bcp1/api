import traceback
from flask_api import status
from src.common.models import ModelImage
from src.common.config import session
from src.common.classes import App_Response, Card


def save_cards(request_json):

    try:

        for json in request_json:

            name, image = json['name'], json['image'].split(',')[1]

            card = Card(name=name, image=image)

            image_model = add_session(card.name, card.image_data_bytes)

            session.add(image_model)

        session.commit()

        message = f"saved {len(request_json)} images"

        response = App_Response(message, status.HTTP_200_OK).make()

    except Exception as e:
        print(traceback.format_exc())
        session.rollback()
        response = App_Response(traceback.format_exc(),
                                status.HTTP_500_INTERNAL_SERVER_ERROR).make()

    finally:

        return response


def add_session(name, bytes):

    image = ModelImage(name, bytes)

    return image
