import base64
from io import BytesIO
import time

import cv2
import numpy as np
from PIL import Image

from src.common.models import ModelImage
from src.common.config import session


class Card():

    def __init__(self, name, image):

        self.image_data_bytes = base64.b64decode(image)
        # self.PIL_image = Image.open(BytesIO(self.image_data_bytes))
        # self.CV_image = np.array(self.PIL_image, dtype=np.uint8)
        self.name = name

    def add_session(self):

        image = ModelImage(self.name, self.image_data_bytes)

        session.add(image)
