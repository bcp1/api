from flask import Response
import json

class App_Response():

    def __init__(self, message, status):

        self.message = json.dumps({'message': message})
        self.status = status

    def make(self):

        return Response(response=self.message, status=self.status)
