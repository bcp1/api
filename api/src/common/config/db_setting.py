import os
from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base
import psycopg2
from src.common.config.app_config import DevelopmentConfig, TestConfig

DATABASE = "postgresql://postgres:postgres@bcp-database:5432/bcp"

if os.getenv("APP_ENV", "develop") == 'develop':
    ENGINE = create_engine(
        DevelopmentConfig.SQLALCHEMY_DATABASE_URI,
        encoding=DevelopmentConfig.SQLALCHEMY_ENCODING,
        echo=DevelopmentConfig.SQLALCHEMY_ECHO,
        connect_args=DevelopmentConfig.SQLALCHEMY_CNNECT_ARGS
    )
else:
    ENGINE = create_engine(
        TestConfig.SQLALCHEMY_DATABASE_URI,
        encoding=TestConfig.SQLALCHEMY_ENCODING,
        echo=TestConfig.SQLALCHEMY_ECHO,
        connect_args=TestConfig.SQLALCHEMY_CNNECT_ARGS
    )

# Sessionの作成
session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=ENGINE
    )
)

Base = declarative_base(bind=ENGINE)
Base.query = session.query_property()
