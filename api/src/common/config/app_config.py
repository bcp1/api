import os


class BaseConfig:

    # SQLAlchemy Connect String
    # postgresql://postgres:postgres@bcp-database:5432/bcp
    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'.format(**{
        'user': os.getenv('DB_USER', 'postgres'),
        'password': os.getenv('DB_PASSWORD', 'postgres'),
        'host': os.getenv('DB_HOST', 'bcp-database'),
        'port': os.getenv('DB_PORT', '5432'),
        'db_name': os.getenv('DB_NAME', 'bcp')
    })
    SQLALCHEMY_ENCODING = 'utf-8'
    SQLALCHEMY_ECHO = False


class DevelopmentConfig(BaseConfig):

    POSTGRES_SCHEMA = 'public'

    SQLALCHEMY_CNNECT_ARGS = {'options': '-csearch_path={}'.format(POSTGRES_SCHEMA)}


class TestConfig(BaseConfig):

    POSTGRES_SCHEMA = 'test'

    SQLALCHEMY_CNNECT_ARGS = {'options': '-csearch_path={}'.format(POSTGRES_SCHEMA)}
