from datetime import datetime
from sqlalchemy import (Column, String, DateTime, BIGINT, LargeBinary)
from src.common.config.db_setting import Base, ENGINE


class ModelImage(Base):
    """
    ImageModel
    """
    __tablename__ = 'Images'
    id = Column('image_id', BIGINT(), primary_key=True, autoincrement=True)
    name = Column('name', String(255), nullable=False)
    binary = Column('binary', LargeBinary(), nullable=False)
    created_at = Column('created_at', DateTime, default=datetime.now, nullable=False)
    updated_at = Column('updated_at', DateTime, default=datetime.now, nullable=False)

    def __init__(self, name, binary):
        self.name = name
        self.binary = binary


if __name__ == "__main__":
    Base.metadata.create_all(bind=ENGINE)
