from flask import Flask
import os
from flask_cors import CORS
from src.common.config import Base, ENGINE
from src.common.config import app_config
from src.controllers import cards

app = Flask('bcp-api')
app.register_blueprint(cards.app)
CORS(app)

if os.getenv('APP_ENV', 'develop') == 'develop':
    app.config.from_object(app_config.DevelopmentConfig)
    app.config['ENV'] = 'development'
else:
    app.config.from_object(app_config.TestConfig)
    app.config['ENV'] = 'test'

app.config['ENGINE'] = ENGINE
app.config['BASE'] = Base

if __name__ == '__main__':
    app.run()
