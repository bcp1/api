from flask import Flask, Blueprint, request

from src.services import save_cards

app = Blueprint('cards', __name__)


@app.route("/cards/reading", methods=["POST"])
def post():

    response = save_cards(request.json)

    return response
